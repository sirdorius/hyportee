﻿using UnityEngine;
using System.Collections;

public class FollowScript : MonoBehaviour {
	public Vector3 offset;

	// Update is called once per frame
	void Update () {
		var pos = Globals.player.transform.position;
		pos += offset;

		iTween.MoveUpdate(gameObject, iTween.Hash(
			"x", pos.x,
			"y", pos.y,
			"z", pos.z,
			"time", 0.5f));
	}
}
