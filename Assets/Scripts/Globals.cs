using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Globals : MonoBehaviour {
	public static bool controlledByKeyboard = false;
	public static string screenshotPath;
	public static bool isGameOver = false;
	
	public static Material playingBG;
	public static Material screenshotBG;
	
	public const int maxShapes = 5;
	public const int maxColors = 7;
	static public bool tutorial = false;
	 
	//density Modifier
	public static int dMod = 5;
	
	public static int bonusLife = 10;
	
	private static int[] numberOfShapes = null;
	private static int[] numberOfColors = null;
	private static int[] shapesGoal = new int[maxShapes];
	private static int[] colorsGoal = new int[maxColors];
	private static List<Room> roomList = new List<Room>();
	private static Room currentRoom;
	private static int rooms = 0;
	
	public static GameObject katamari;
	public static GameObject player;
	public static GameObject mainLight;
	public static GameObject katamariLight;
	public static GameObject goal;
	
	public enum Shapes {Triangle = 0, Square, Circle, Pentagon, Rhombus};
	public enum Colors {Aqua = 0, Black, Blue, Green, Lilla, Res, Yellow};
		
	public static void updateGoalShapes(int[] n) {
		if(numberOfShapes == null) {
			numberOfShapes = new int[maxShapes];
			for(int i=0;i < maxShapes;i++)
				shapesGoal[i] = 0;
		}
		for(int i=0;i < maxShapes;i++)
			shapesGoal[i] = n[i];
	}
	
	public static void updateGoalColors(int[] n) {
		if(numberOfColors == null) {
			numberOfColors = new int[maxColors];
			for(int i=0;i < maxColors;i++)
				colorsGoal[i] = 0;
		}
		
		for(int i=0;i < maxColors;i++)
			colorsGoal[i] = n[i];
	}
	
	public static void addShape(Shapes s) {
		numberOfShapes[(int) s]++;
		player.GetComponent<Player>().changeArrow(!goalReach());
	}
	
	public static void addColor(Colors c) {
		numberOfColors[(int) c]++;
	}
	
	public static bool goalReach() {
		bool check = true;
		for(int i=0;i < maxShapes;i++)
			check = check && (numberOfShapes[i] >= shapesGoal[i]);
		for(int i=0;i < maxColors;i++)
			check = check && (numberOfColors[i] >= colorsGoal[i]);
		return check;
	}
	
	public static void addRoom(float centerx, float centery, float height, float width) {
		roomList.Add(new Room(centerx, centery, height, width));
	}
	
	public static IEnumerator next() {
		GameObject enemies = GameObject.Find("Enemies");
		
		if(rooms != 0) {
			player.GetComponent<Player>().changeLife(bonusLife);
			Destroy(katamariLight);
			Destroy(katamari.GetComponent<Character>());
			Destroy(katamari.rigidbody);
			Destroy(katamari.collider);
			
			GameObject oldEnemies = GameObject.Find("OldEnemies");
			
			for(int i=0;i < enemies.transform.GetChildCount();i++) {
				GameObject g = enemies.transform.GetChild(i).gameObject;
				Destroy(g.rigidbody);
				Destroy(g.GetComponent<Character>());
				Destroy(g.collider);
				Destroy(g.GetComponent<TrailRenderer>());
				g.transform.parent = oldEnemies.transform;
			}
		}
		rooms++;
		currentRoom = roomList[0];
		roomList.RemoveAt(0);
		
		katamari = (GameObject) Instantiate(Resources.Load("Katamari"));
		katamari.rigidbody.isKinematic = true;
		katamari.transform.position = currentRoom.randomPoint();
		katamariLight = katamari.transform.FindChild("KatamariLight").gameObject;
		goal.transform.position = currentRoom.randomPoint();
		
		player.GetComponent<Player>().changeArrow();
		
		for(int i=0;i < maxShapes;i++)
			numberOfShapes[i] = 0;
		for(int i=0;i < maxColors;i++)
			numberOfColors[i] = 0;
			
		int[] objectiveShapes = new int[maxShapes];
		foreach(Object go in Resources.LoadAll("Enemies")) {
			GameObject g = (GameObject) Instantiate(go);
			int nShapes = 0;
			Shapes t = g.GetComponent<Enemy>().myType;
			switch(t) {
			case Shapes.Circle:
			case Shapes.Square:
				nShapes = (int) (4*dMod*rooms*0.2*currentRoom.size);
				objectiveShapes[(int) t] = (int) (0.1*nShapes);
				break;
			case Shapes.Pentagon:
				nShapes = (int) (dMod*rooms*0.2*currentRoom.size);
				objectiveShapes[(int) t] = (int) (0.8*nShapes);
				break;
			case Shapes.Rhombus:
				nShapes = (int) (2*dMod*rooms*0.2*currentRoom.size);
				objectiveShapes[(int) t] = (int) (0.5*nShapes);
				break;
			case Shapes.Triangle:
				nShapes = (int) (2*dMod*(rooms-1)*0.2*currentRoom.size);
				objectiveShapes[(int) t] = (int) (0.7*nShapes);
				break;
			}
			g.transform.position = currentRoom.randomPoint();
			g.transform.localScale = g.transform.localScale*Random.Range(1.1f, 1.5f);
			g.transform.parent = enemies.transform;
			
			for(int i=0; i < nShapes-1; i++) {
				g = (GameObject) Instantiate(go);
				g.transform.position = currentRoom.randomPoint();
				g.transform.localScale = g.transform.localScale*Random.Range(1f, 1.7f);
				g.transform.parent = enemies.transform;
			}
		}
		updateGoalShapes(objectiveShapes);
				
		// animation
		Vector3 p = new Vector3(currentRoom.center.x, currentRoom.center.y, Camera.main.transform.position.z);
		iTween.MoveTo(Camera.main.gameObject, p, 3f);
		iTween.ValueTo(Camera.main.gameObject, iTween.Hash("onupdate", "zoomCamera",
			"from", Camera.main.orthographicSize, "to", Camera.main.orthographicSize*3, "time", 1.5f,
			"easeType", "easeOutSine"));
		iTween.ValueTo(Camera.main.gameObject, iTween.Hash("onupdate", "zoomCamera",
			"from", Camera.main.orthographicSize*3, "to", 15, "time", 1.5f,
			"easeType", "easeInOutSine", "delay", 1.5f));
		
		player.rigidbody.velocity = Vector3.zero;
		player.GetComponent<Player>().enabled = false;
		player.transform.GetChild(0).gameObject.SetActive(false);
		player.transform.GetChild(1).gameObject.SetActive(false);
		player.transform.position = currentRoom.center;
		player.SetActive(false);
		yield return new WaitForSeconds(3f);
		player.SetActive(true);
		iTween.ScaleFrom(player, iTween.Hash("scale", Vector3.zero, "time", 1.5f));
		yield return new WaitForSeconds(1.5f);
		player.GetComponent<Player>().enabled = true;
		player.transform.GetChild(0).gameObject.SetActive(true);
		player.transform.GetChild(1).gameObject.SetActive(true);
		katamari.rigidbody.isKinematic = false;
	}
	
	public static void levelGenerated() {
		roomList.Sort();
	}
	
	public static void takeScreenShot() {
		
		int superSize = 3;
		int width = Screen.width*superSize;
		int height = Screen.height*superSize;
		string name = string.Format("screenshots/screen_{3}.png",
			Application.dataPath,
			width, height,
			System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
		Application.CaptureScreenshot(name, superSize);
		screenshotPath = "On the bright side though, we saved you a screenshot in\n"+name+"\n\nPress any key to go back to the menu";
		
		//screenshotPath = "Download the desktop version to get\nan awesome screenshot of your level";
	}
	
	public static void gameOver() {
		float zoomToSpeed = 3;
		switchCameraFollowsPlayer();
		iTween.ValueTo(Camera.main.gameObject, iTween.Hash("onupdate", "zoomCamera",
			"from", Camera.main.orthographicSize, "to", Camera.main.orthographicSize * 3, "time", zoomToSpeed,
			"easeType", "easeOutSine"));
		/*iTween.ValueTo(Camera.main.gameObject, iTween.Hash("onupdate", "zoomCamera",
			"from", Camera.main.orthographicSize*3, "to", Camera.main.orthographicSize, "time", zoomToDuration/2f,
			"easeType", "circIn", "delay", zoomToDuration/2f));*/
		iTween.MoveTo(Camera.main.gameObject, iTween.Hash("position", new Vector3(0,0,Camera.main.transform.position.z),
			"time", zoomToSpeed, "easeType", "easeOutSine", "oncomplete", "zoomCompleted"));
		var ps = GameObject.Find("Planes");
		foreach(Transform i in ps.transform) {
			if (i.renderer.material.color == playingBG.color) {
				i.renderer.material = screenshotBG;
			}
		}
		Instantiate(Resources.Load("finalLight"));
		Destroy(GameObject.Find("Light"));
		Destroy(Globals.katamariLight);
	}
	
	public static void switchCameraFollowsPlayer() {
		var c = Camera.main.GetComponent<CameraScript>();
		c.followingPlayer = !c.followingPlayer;
	}
	
	public static int[] updateGUI() {
		int[] objective = new int[maxShapes];
		for(int i=0; i < maxShapes;i++) {
			objective[i] = shapesGoal[i] - numberOfShapes[i];
			if (objective[i] < 0) { objective[i] = 0; }
		}
		return objective;
	}
	
	public static void reset() {
		rooms = 0;
		roomList.Clear();
		isGameOver = false;
		tutorial = false;
	}
	
	public static void newPosition() {
		Vector3 dir = currentRoom.randomPoint();
		while(Physics.Raycast(new Vector3(dir.x, dir.y, dir.z - 50) , dir))
			dir = currentRoom.randomPoint();
		player.transform.position = dir;
		dir = currentRoom.randomPoint();
		while(Physics.Raycast(new Vector3(dir.x, dir.y, dir.z - 50) , dir))
			dir = currentRoom.randomPoint();
		goal.transform.position = dir;
		player.GetComponent<Player>().changeLife(-5);
	}
		
}
