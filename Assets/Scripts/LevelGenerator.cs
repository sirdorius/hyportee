using UnityEngine;
using System.Collections;

public class LevelGenerator : MonoBehaviour {
	
	GameObject walls;
	GameObject planes;
	
	private void initWalls() {
		GameObject go = (GameObject) Instantiate(Resources.Load("blackLine"));
		go.transform.position = new Vector3(0,-200,0);
		go.transform.localScale = new Vector3(1.5f,403,1);
		go.transform.parent = walls.transform; 
		go = (GameObject) Instantiate(Resources.Load("blackLine"));
		go.transform.position = new Vector3(0, 200, 0);
		go.transform.localScale = new Vector3(1.5f,403,1);
		go.transform.parent = walls.transform;
		go = (GameObject) Instantiate(Resources.Load("blackLine"));
		go.transform.position = new Vector3(-200, 0, 0);
		go.transform.localScale = new Vector3(403, 1.5f,1);
		go.transform.parent = walls.transform;
		go = (GameObject) Instantiate(Resources.Load("blackLine"));
		go.transform.position = new Vector3(200, 0, 0);
		go.transform.localScale = new Vector3(403, 1.5f,1);
		go.transform.parent = walls.transform;
	}
	
	private void split(int minDist, int maxDist, int nextMinDist, int nextMaxDist, int initialPos,int width, bool vertical) {
		if(((Mathf.Abs(minDist - maxDist) < 100) && Random.value > 0.7f) || (Mathf.Abs(minDist - maxDist) < 50)) {
			GameObject g = (GameObject) Instantiate(Resources.Load("plane"));
			g.transform.parent = planes.transform;
			if(vertical) {
				g.transform.position = new Vector3((minDist + maxDist)/2 , initialPos, 5);
				g.transform.localScale = new Vector3(maxDist - minDist, nextMaxDist - nextMinDist, 0.1f);
				Globals.addRoom((minDist + maxDist)/2, initialPos, nextMaxDist - nextMinDist, maxDist - minDist);
			}
			else {
				g.transform.position = new Vector3(initialPos, (minDist + maxDist)/2, 5);
				g.transform.localScale = new Vector3(nextMaxDist - nextMinDist, maxDist - minDist, 0.1f);
				Globals.addRoom(initialPos, (minDist + maxDist)/2, maxDist - minDist, nextMaxDist - nextMinDist);
			}
			if(Random.value > 0.7f) {
				float val = Random.value;
				if(val > 0 && val <= 0.3f)
					g.GetComponentInChildren<Renderer>().material = (Material) Resources.Load("Materials/RedBg");
				if(val > 0.3f && val <= 0.6f)
					g.GetComponentInChildren<Renderer>().material = (Material)Resources.Load("Materials/BluBg");
				if(val > 0.6f)
					g.GetComponentInChildren<Renderer>().material = (Material)Resources.Load("Materials/YellowBg");
			}
			return;
		}
		int position = Random.Range(minDist + 20, maxDist - 20);
		GameObject go = (GameObject) Instantiate(Resources.Load("blackLine"));
		if(vertical) {
			go.transform.position = new Vector3(position, initialPos, 0);
			go.transform.localScale = new Vector3(width, 2, 1);
		} else {
			go.transform.position  = new Vector3(initialPos, position, 0);
			go.transform.localScale = new Vector3(2, width, 1);
		}
		go.transform.parent = walls.transform;
		split (nextMinDist, nextMaxDist, minDist, position, (minDist + position)/2, Mathf.Abs(minDist - position), !vertical);
		split (nextMinDist, nextMaxDist, position, maxDist, (position + maxDist)/2, Mathf.Abs(position - maxDist), !vertical);
	}

	// Use this for initialization
	void Awake() {
		walls = GameObject.Find("Walls");
		planes = GameObject.Find("Planes");
		initWalls();
		split (-200, +200, -200, +200, 0, 400, true);
		Globals.levelGenerated();
	}
}
