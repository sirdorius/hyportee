using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
	public Font smallFont;
	public Font bigFont;
	public Font tinyFont;
	public bool tutorial = false;
	
	void Start() {	
		Globals.katamari = GameObject.Find("Katamari");
		Globals.player = GameObject.Find("Player");
		Globals.mainLight = GameObject.Find("Light");
		Globals.katamariLight = GameObject.Find("KatamariLight");
		Camera.main.transparencySortMode = TransparencySortMode.Orthographic;
	}
	
	void Update() {
		if (Input.GetMouseButtonDown(0)) {
			RaycastHit hit = new RaycastHit();
			if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 1000, 1<<11)) {
				switch(hit.collider.name) {
				case "PlayButton":
					iTween.MoveTo(Camera.main.gameObject, iTween.Hash("position", new Vector3(50f, 0, Camera.main.transform.position.z),
							"time", 1f));
					Globals.tutorial = true;
					break;
				case "MusicButton":
					var a = GameObject.Find("Audio").GetComponent<AudioSource>();
					a.enabled = !a.enabled;
					break;
				case "ExitButton":
					Application.Quit();
					break;
				case "ExitTutorial":
					Application.LoadLevel(1);
					break;
				}
			}
		}
	}
	
	/*void OnGUI() {
		if (Globals.tutorial) return;
		int left = 75;
		int top = 75;
		
		GUI.skin.font = bigFont;
		GUI.Label(new Rect(left-25, 25, 600,300), "AntiGrad");
		
		GUI.skin.font = tinyFont;
		GUI.Label(new Rect(left-25, 150, 300,300), "Ludum Dare 26 entry\nTheme: Minimalism");
		
		GUI.Label(new Rect(left-25, Screen.height - 150, 300,300), "A game by\nDorian Bucur\nGianluca Bardaro\n" +
			"Music: Hemostatic Misantrophy\n" +
			"by Gabriele Civitarese");
	}*/
}
