using UnityEngine;
using System.Collections;

public class Hugger : Enemy {
	
	void Awake() {
		myType = Globals.Shapes.Pentagon;
	}
	
	void OnTriggerStay(Collider c) {
		if (attractedToKatamari) return;
		if (c.name == "VicinityTriggerFar" && rigidbody) {
			var p = c.gameObject.transform.position - transform.position;
			p *= acceleration*(15-p.magnitude);
			rigidbody.AddForce(p, ForceMode.Acceleration);
			rigidbody.velocity = Vector3.ClampMagnitude (rigidbody.velocity, maxSpeed);
		}
	}
	
	public override void playerIsInRange() {
	}
}
