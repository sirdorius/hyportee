using UnityEngine;
using System.Collections;

public class Square : Enemy {
	
	void Awake() {
		myType = Globals.Shapes.Square;
	}
	
	public void playerIsInRange(GameObject player) {
	}

	public override void playerIsInRange() {
	}
	
	void OnTriggerStay(Collider c) {
		if (c.tag == "Katamari") {
			rigidbody.AddForce((c.gameObject.transform.position - transform.position).normalized*acceleration, ForceMode.Acceleration);
		}
	}
}
