using UnityEngine;
using System.Collections;

public class KatamariMenu : Katamari {
	void Start() {
		base.isSticky = State.Sticky;
	}
	
	void OnCollisionEnter(Collision c) {
		stickObject(c);
	}
	
	void OnCollisionStay(Collision c) {
		stickObject(c);
	}
	
	override protected void stickObject(Collision c) {
		if (isSticky != State.Sticky) return;
		var enemy = c.gameObject.GetComponent<Enemy>();
		if (enemy && !enemy.attractedToKatamari) {
			enemy.katamariAttractedBy = Globals.katamari;
			enemy.attractToKatamari(attractionForceToKatamari, 2);
			Globals.katamari.rigidbody.mass += massAddedPerObject;
			c.gameObject.AddComponent("KatamariMenu");
			Destroy(c.gameObject.GetComponent<TrailRenderer>());
		}
	}
}
