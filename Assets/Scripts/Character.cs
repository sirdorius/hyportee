using UnityEngine;
using System.Collections;

public class Character : MonoBehaviour {
	public float acceleration;
	private float blinkDuration = 0.3f;
	private float fadeMinAlpha = 0.3f;
	private Vector3 initialScale;
	//private Color initialLight;
	
	private bool blink = false;
	private bool fade = false;
	
	public void blinkAnimation(float s) {
		if (blink) return;
		blink = true;
		initialScale = transform.localScale;
		iTween.ScaleTo(gameObject, iTween.Hash("scale", initialScale*s, "time", blinkDuration));
		iTween.ScaleTo(gameObject, iTween.Hash("scale", initialScale, "time", blinkDuration, "delay", blinkDuration));
		StartCoroutine(blinkFinished(blinkDuration));
	}
	
	public void fadeAnimation() {
		if (fade) return;
		fade = true;
		
		// TODO buggato
		//iTween.ColorTo(Globals.katamariLight, iTween.Hash ("color", initialLight, "time", 0.1f));
		//iTween.ColorFrom(Globals.katamariLight, iTween.Hash ("color", new Color(0,1,0,1f), "time", 0.1f, "delay", 0.1f));
		
		iTween.FadeFrom(Globals.katamari, iTween.Hash("value", fadeMinAlpha, "time", blinkDuration, "delay", blinkDuration, "oncomplete", "fadeFinished"));
		iTween.FadeTo(Globals.katamari, fadeMinAlpha, blinkDuration);
	}
	
	void fadeFinished() {
		fade = false;
	}
	
	IEnumerator blinkFinished(float t) {
		yield return new WaitForSeconds(t*2);
		blink = false;
	}
}
