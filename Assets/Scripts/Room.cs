using UnityEngine;
using System;
using System.Collections.Generic;

public class Room : IComparable<Room> {
	
	private const float minSize = 20;
	
	public Vector3 diagonal { get; set; }
	public Vector3 center { get; set; }
	public float size { get; set; }
	
	public int CompareTo(Room other) {
		float dist = Vector3.Distance(Vector3.zero, center);
		float otherdist = Vector3.Distance(Vector3.zero, other.center);
		return dist.CompareTo(otherdist); 
	}
	
	public Room(float centerx, float centery, float height, float width) {
		center = new Vector3(centerx, centery, 0);
		diagonal = new Vector3(width/2, height/2, 0);
		size = diagonal.magnitude / minSize;
	}
	
	public Vector3 randomPoint() {
		float coordx, coordy;
		if(UnityEngine.Random.value <= 0.5f)
			coordx = center.x + UnityEngine.Random.Range(diagonal.x*0.1f, diagonal.x*0.9f);
		else
			coordx = center.x - UnityEngine.Random.Range(diagonal.x*0.1f, diagonal.x*0.9f);
		if(UnityEngine.Random.value <= 0.5f)
			coordy = center.y + UnityEngine.Random.Range(diagonal.y*0.1f, diagonal.y*0.9f);
		else
			coordy = center.y - UnityEngine.Random.Range(diagonal.y*0.1f, diagonal.y*0.9f);
		return new Vector3(coordx, coordy , 0);
	}
}
