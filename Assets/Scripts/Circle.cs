using UnityEngine;
using System.Collections;

public class Circle : Enemy {
	
	void Awake() {
		myType = Globals.Shapes.Circle;
	}
	
	void OnTriggerStay(Collider c) {
		if (attractedToKatamari) return;
		if (c.name == "VicinityTriggerNear" && rigidbody)
			rigidbody.AddForce((transform.position - c.gameObject.transform.position).normalized*acceleration, ForceMode.Acceleration);
	}
	
	void OnTriggerEnter(Collider c) {
		if (c.name == "VicinityTriggerNear" && rigidbody) {
			blinkAnimation(1.2f);
		}
	}
	
	public override void playerIsInRange() {
	}
}