using UnityEngine;
using System.Collections;

public class GoalGUI : MonoBehaviour {
	
	private Object[] icons;
	public Font font;
	private GUIStyle style;
	private int position;
	
	void Awake() {
		icons = Resources.LoadAll("Icons");
		position = Screen.width/2 - 200;
		style = new GUIStyle();
		style.font = font;
		style.normal.textColor = Color.white;
		style.fixedHeight = 45;
		style.fixedWidth = 45;
	}
		
	void OnGUI() {
		GUI.skin.label.normal.textColor = Color.white;
		if (Globals.player.GetComponent<Player>().health > 0) {
			int[] objective = Globals.updateGUI();
			for(int i=0;i<Globals.maxShapes;i++) {
				GUI.Label(new Rect(position+i*100,20,600,70), new GUIContent(objective[i].ToString(), (Texture2D) icons[i]), style);
			}
		}
	}
}
