using UnityEngine;
using System.Collections;

public abstract class Enemy : Character {
	
	public Globals.Shapes myType;
	public float maxSpeed;
	
	public bool attractedToKatamari = false;
	private float accelToKatamari;
	public GameObject katamariAttractedBy = null;
	
	private float STOP_THRESHOLD = 0.1f;
	
	public void attractToKatamari(float accel, float t) {
		transform.parent = Globals.katamari.transform;
		rigidbody.velocity /= 2;
		attractedToKatamari = true;
		accelToKatamari = accel;
		
		Globals.katamari.GetComponent<Character>().fadeAnimation();
	}
	
	abstract public void playerIsInRange();
	
	void Start() {
		var angle = Random.Range(0,360);
		var acceleration = new Vector2(Mathf.Cos(angle*Mathf.Deg2Rad), Mathf.Sin(angle*Mathf.Deg2Rad));
		if (rigidbody)
			rigidbody.AddForce(acceleration*Random.Range(maxSpeed/2, maxSpeed));
	}
	
	protected void FixedUpdate() {
		if (rigidbody && attractedToKatamari) {
			if (rigidbody.velocity.sqrMagnitude < STOP_THRESHOLD) {
				stopInPlace();
			}	
			else {
				var v = (katamariAttractedBy.transform.position - transform.position)*accelToKatamari;
				v *= v.magnitude;
				v = Vector3.ClampMagnitude(v, 75f);
				rigidbody.AddForce(v, ForceMode.Acceleration);
			}
		}
		rigidbody.velocity = Vector3.ClampMagnitude(rigidbody.velocity, maxSpeed);
	}
	
	public void stopInPlace() {
		Destroy(rigidbody);
		Destroy(gameObject.GetComponent<Enemy>());
	}
}
