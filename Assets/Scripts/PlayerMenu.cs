using UnityEngine;
using System.Collections;

public class PlayerMenu : Player {
	
	override public void loseLife(int i) {
	}
	
	void Update() {
		if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButton(0)) {
			Globals.katamari.GetComponent<Katamari>().becomeSticky();
		}
		if (Input.GetKeyDown(KeyCode.Tab)) {
			Globals.controlledByKeyboard = !Globals.controlledByKeyboard;
		}
	}
	
	void FixedUpdate () {
		if (Globals.tutorial) return;
		float deltaX, deltaY;
		Vector2 p = (Camera.main.ScreenToWorldPoint(Input.mousePosition) - Globals.player.transform.position);
		p = Vector3.ClampMagnitude(p, 1);
		p = p*velocity*Time.deltaTime;
		deltaX = p.x;
		deltaY = p.y;
		
		rigidbody.AddForce(new Vector3(deltaX, deltaY, 0), ForceMode.VelocityChange);
	}
	
	void OnGUI() {
		
	}
}
