using UnityEngine;
using System.Collections;

public class Goal : Character {

	void Awake () {
		Globals.goal = gameObject;
	}
		
	void OnCollisionEnter(Collision c) {
		if(c.collider.name == "Player") {
			if(Globals.goalReach())
				StartCoroutine(Globals.next());
			return;
		}
	}
}
