using UnityEngine;
using System.Collections;

public class InitializeLevel : MonoBehaviour {

	// Use this for initialization
	void Start () {
		int[] shape = new int[Globals.maxShapes] {0, 0, 0, 0, 0};
		int[] color = new int[Globals.maxColors] {0, 0, 0, 0, 0, 0, 0};
		Globals.updateGoalColors(color);
		Globals.updateGoalShapes(shape);
		Globals.player = GameObject.Find("Player");
		Globals.mainLight = GameObject.Find("Light");
		Camera.main.transparencySortMode = TransparencySortMode.Orthographic;
		StartCoroutine(Globals.next());
		Globals.playingBG = (Material)Resources.Load("Materials/DarkGrey");
		Globals.screenshotBG = (Material)Resources.Load("Materials/White");
	}
}
