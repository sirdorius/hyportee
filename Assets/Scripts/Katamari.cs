using UnityEngine;
using System.Collections;

public class Katamari : Character {
	public float stickyDuration;
	public float rechargeDuration;
	
	public float massAddedPerObject = 0.1f;
	public float rangeAddedPerObject = 1f;
	
	protected float attractionForceToKatamari = 20;
	protected enum State {Ready, Sticky, Recharging};
	protected State isSticky = State.Sticky;
	
	void OnCollisionEnter(Collision c) {
		stickObject(c);
	}
	
	void OnCollisionStay(Collision c) {
		stickObject(c);
	}
	
	virtual protected void stickObject(Collision c) {
		if (isSticky != State.Sticky) return;
		var enemy = c.gameObject.GetComponent<Enemy>();
		if (enemy && !enemy.attractedToKatamari) {
			Globals.addShape(enemy.myType);
			enemy.katamariAttractedBy = Globals.katamari;
			enemy.attractToKatamari(attractionForceToKatamari, 2);
			Globals.katamari.rigidbody.mass += massAddedPerObject;
			Globals.katamari.GetComponent<SphereCollider>().radius += rangeAddedPerObject;
			c.gameObject.AddComponent("Katamari");
			Destroy(c.gameObject.GetComponent<TrailRenderer>());
		}
	}
	
	public void becomeSticky() {
		if (isSticky == State.Ready) {
			iTween.ColorTo(gameObject, iTween.Hash ("color", new Color(0.2f,0.7f,0.2f,1), "time", 0.4f, "includechildren", false));
			isSticky = State.Sticky;
			StartCoroutine(rechargeSticky());
		}
	}
	
	IEnumerator rechargeSticky() {
		// is sticky
		yield return new WaitForSeconds(stickyDuration);
		// starts recharging
		iTween.ColorTo(gameObject, iTween.Hash ("color", new Color(0.7f,0.2f,0.2f,1), "time", 0.4f, "includechildren", false));
		isSticky = State.Recharging;
		yield return new WaitForSeconds(rechargeDuration);
		// finished recharging
		iTween.ColorTo(gameObject, iTween.Hash ("color", new Color(0,0,0,1), "time", 0.4f, "includechildren", false));
		isSticky = State.Ready;
	}
}
