using UnityEngine;
using System.Collections;

public class Rhombus : Enemy {
	
	void Awake() {
		myType = Globals.Shapes.Rhombus;
	}
	
	void OnTriggerStay(Collider c) {
		if (attractedToKatamari) return;
		if (c.name == "VicinityTriggerFar" && rigidbody) {
			Vector3 p = c.gameObject.transform.position - transform.position;
			rigidbody.AddForce(p.normalized*acceleration*2.75f, ForceMode.Acceleration);
			p = new Vector2(p.y, -p.x);
			rigidbody.AddForce(p.normalized*acceleration, ForceMode.Acceleration);
			rigidbody.velocity = Vector3.ClampMagnitude(rigidbody.velocity, maxSpeed);
		}
		if (c.name == "VicinityTriggerNear" && rigidbody) {
			rigidbody.AddForce((transform.position-c.transform.position)*1.3f, ForceMode.Acceleration);
		}
	}
	
	public override void playerIsInRange() {
	}
}
