using UnityEngine;
using System.Collections;

public class Player : Character {
	public int health = 100;
	public Font lifeFont;
	
	private Texture2D heart;
	
	public Transform arrow;

	public float velocity = 700;
	public float mouseVelocityRange = 0.4f;
	
	private float ratio = Screen.width/Screen.height;
	
	private bool invincible;
	private bool reset = false;
	
	private float invincibileDuration = 1.0f;
	private float resetCooldown = 5.0f;
	private bool arrowPointsToObjective = false;
	
	public void Awake() {
		heart = (Texture2D) Resources.Load("Icons/99heart");
	}
	
	public void changeLife(int i) {
		health += i;
		if (health <= 0) {
			gameObject.SetActive(false);
			Globals.gameOver();
			return;
		}
	}
	
	public virtual void loseLife(int i) {
		if (invincible) return;
		health -= i;
		if (health <= 0) {
			gameObject.SetActive(false);
			Globals.gameOver();
			return;
		}
		invincible = true;
		iTween.ColorTo(Globals.mainLight, iTween.Hash ("color", new Color(0.5f,0.2f,0.2f,1), "time", 0.2f));
		StartCoroutine(stopInvinicibility());
	}
	
	IEnumerator stopInvinicibility() {
		yield return new WaitForSeconds(invincibileDuration);
		invincible = false;
		iTween.ColorTo(Globals.mainLight, iTween.Hash ("color", new Color(1,1,1,1), "time", 0.2f));
	}
	
	IEnumerator activeReset() {
		yield return new WaitForSeconds(resetCooldown);
		reset = false;
	}
	
	public void changeArrow(bool direction){
		arrowPointsToObjective = direction;
	}
	
	public void changeArrow(){
		arrowPointsToObjective = !arrowPointsToObjective;
	}
	
	void Update() {
		/*if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButton(0)) {
			Globals.katamari.GetComponent<Katamari>().becomeSticky();
		}*/
		if (Input.GetKeyDown(KeyCode.Tab)) {
			Globals.controlledByKeyboard = !Globals.controlledByKeyboard;
		}
		var s = Input.GetAxis("Mouse ScrollWheel");
		if (s!=0) {
			Camera.main.orthographicSize -= s*(Camera.main.orthographicSize);
		}
		if (Input.GetKeyDown(KeyCode.P)) {
			Globals.takeScreenShot();
		}
		if (Input.GetKeyDown(KeyCode.R)) {
			Globals.reset();
			Application.LoadLevel(1);
		}
		if (Input.GetKeyDown(KeyCode.Q) && !reset) {
			Globals.newPosition();
			reset = true;
			StartCoroutine(activeReset());
		}
		
		//transform.LookAt(transform.position+rigidbody.velocity, new Vector3(0,0,1));
		if (arrowPointsToObjective) {
			arrow.LookAt(Globals.katamari.transform.position, new Vector3(0,0,1));
		}
		else {
			arrow.LookAt(Globals.goal.transform.position, new Vector3(0,0,1));
		}
		arrow.Rotate(0,180,0);
	}
	
	void FixedUpdate () {;
		float deltaX, deltaY;
		if (!Globals.controlledByKeyboard) {
			Vector2 p = ((Vector2)Camera.main.ScreenToViewportPoint(Input.mousePosition) -
					new Vector2(0.5f,0.5f));
			p = new Vector2(p.x*ratio, p.y);
			p /= mouseVelocityRange;
			p = Vector3.ClampMagnitude(p, 1);
			p = p*velocity*Time.deltaTime;
			deltaX = p.x;
			deltaY = p.y;
		}
		else {
			// TODO tween scale when keys pressed
			deltaY = Input.GetAxis("Vertical")*velocity*Time.deltaTime;
			deltaX = Input.GetAxis("Horizontal")*velocity*Time.deltaTime;
		}
		
		rigidbody.AddForce(new Vector3(deltaX, deltaY, 0), ForceMode.VelocityChange);
	}
	
	void OnTriggerEnter(Collider c) {
		Enemy e = c.GetComponent<Enemy>();
		if (e != null) e.playerIsInRange();
	}
	
	void OnGUI() {
		GUI.skin.label.normal.textColor = Color.white;
		GUI.skin.font = lifeFont;
		GUI.Label(new Rect(30,0,600,600), new GUIContent(Globals.player.GetComponent<Player>().health.ToString(), heart));
	}
}
