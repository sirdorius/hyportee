using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour {
	private  new GameObject light;
	public bool followingPlayer = true;
	public Font fontBig, fontSmall;
	
	void Start() {
		light = GameObject.Find("Light");
	}
	
	// Update is called once per frame
	void Update () {
		if (Globals.isGameOver) {
			var s = Input.GetAxis("Mouse ScrollWheel");
			if (s!=0) {
				Camera.main.orthographicSize -= s*(Camera.main.orthographicSize);
			}
		}
		if (!followingPlayer) return;
		Camera.main.transform.position = new Vector3(Globals.player.transform.position.x,
					Globals.player.transform.position.y, Camera.main.transform.position.z);
	}
	
	public void zoomCamera(float f) {
		Camera.main.orthographicSize = f;
	}
	
	public void zoomCompleted() {
		Globals.takeScreenShot();
		StartCoroutine(gameOverCor());
	}
	
	IEnumerator gameOverCor() {
		yield return new WaitForSeconds(2f);
		Globals.isGameOver = true;
	}
	
	void OnGUI() {
		if (Globals.isGameOver) {
			GUI.skin.label.normal.textColor = new Color(0f,0f,0.2f,1f);
			GUI.skin.font = fontSmall;
			GUI.Label(new Rect(100,Screen.height-150,1000,300), Globals.screenshotPath);
			GUI.skin.font = fontBig;
			GUI.Label(new Rect(Screen.width/2-150,Screen.height/2-50,1000,300), "Game Over");
			if(Input.anyKey) {
				Application.LoadLevel(0);
				Globals.reset();
			}
		}
	}
}
