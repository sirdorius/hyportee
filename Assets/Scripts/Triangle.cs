using UnityEngine;
using System.Collections;

public class Triangle : Enemy {
	public int dmgToPlayer;
	
	void Awake() {
		myType = Globals.Shapes.Triangle;
	}
	
	public override void playerIsInRange() {
		if (attractedToKatamari) return;
		rigidbody.AddForce((Globals.player.transform.position - transform.position).normalized*acceleration, ForceMode.Acceleration);
	}
	
	void OnTriggerEnter(Collider c) {
		if (attractedToKatamari) return;
		if (c.name == "VicinityTriggerFar" && rigidbody) {
			blinkAnimation(1.2f);
		}
		if (c.tag == "Katamari") {
			rigidbody.AddForce((transform.position - c.gameObject.transform.position).normalized*acceleration/2, ForceMode.Acceleration);
		}
	}
	
	void OnCollisionEnter(Collision c) {
		if (c.collider.tag == "Player") {
			c.gameObject.GetComponent<Player>().loseLife(dmgToPlayer);
		}
	}
	
	void Update() {
		if (attractedToKatamari) return;
		transform.LookAt(Globals.player.transform, new Vector3(0,0,1));
		transform.Rotate(0,-135,0);
	}
}
